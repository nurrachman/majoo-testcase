import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:oktoast/oktoast.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void createDatebase() async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, DatabaseLocal.DB_NAME);

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute(
          'CREATE TABLE ${DatabaseLocal.TBL_USER} (id INTEGER PRIMARY KEY, email TEXT, password TEXT, username Text)');
      await db.transaction((txn) async {
        int id1 = await txn.rawInsert(
            'INSERT INTO ${DatabaseLocal.TBL_USER}(email, password, username) VALUES("nurrachmen@gmail.com", "1234", "Rachman")');
        print('${DatabaseLocal.TBL_USER} 1 Inserted: $id1');
      });
    });

    await database.close();
  }

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState(true));
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async {
    emit(AuthBlocInitialState());
    var databasePath = await getDatabasesPath();
    var responseUser = User();
    String path = join(databasePath, DatabaseLocal.DB_NAME);

    Database database = await openDatabase(path, version: 1);
    List<Map> list = await database.rawQuery(
        'SELECT * FROM ${DatabaseLocal.TBL_USER} WHERE email = ? AND password = ?',
        [
          user.email.toLowerCase(),
          user.password,
        ]);

    list.forEach((element) {
      responseUser.email = element["email"];
      responseUser.password = element["password"];
      responseUser.userName = element["username"];
    });

    await database.close();

    var message = "Login gagal , periksa kembali inputan anda";

    if (list.length > 0) {
      message = "Login Berhasil";
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      emit(AuthBlocLoadingState());
      await sharedPreferences.setBool("is_logged_in", true);
      String data = responseUser.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState(true));
    }

    showToast(message);
  }

  void register_user(User user) async {
    emit(AuthBlocInitialState());
    var databasePath = await getDatabasesPath();
    var responseUser = User();
    String path = join(databasePath, DatabaseLocal.DB_NAME);

    Database database = await openDatabase(path, version: 1);
    await database.transaction((txn) async {
      int id1 = await txn.rawInsert(
          'INSERT INTO ${DatabaseLocal.TBL_USER}(email, password, username) VALUES(?, ?, ?)',
          [
            user.email.toLowerCase(),
            user.password,
            user.userName,
          ]);
      if (id1 > 0) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        emit(AuthBlocLoadingState());
        await sharedPreferences.setBool("is_logged_in", true);
        String data = responseUser.toJson().toString();
        sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState(true));

        showToast("Register Berhasil");
      }
    });

    await database.close();
  }
}
