import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home/home_screen.dart';
import 'package:path/path.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _usernameController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  bool _isObscurePassword = true;
  bool _isPageRegister = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
        cubit: authBlocCubit,
        listener: (context, state) {
          print('state >>> ${state.toString()}');
          print('state >>> $state');
          if (state is AuthBlocLoggedInState) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          return SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan ${_isPageRegister ? 'daftar' : 'login'} terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(context),
                  SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                    text: _isPageRegister ? 'Register' : 'Login',
                    onPressed: _isPageRegister ? handleRegister : handleLogin,
                    height: 100,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  _register(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _form(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Column(
            children: _isPageRegister
                ? [
                    Container(
                      height: 100,
                      child: CustomTextFormField(
                        context: context,
                        controller: _usernameController,
                        isEmail: true,
                        hint: 'Jhon doe',
                        label: 'Username',
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                  ]
                : [],
          ),
          Container(
            height: 100,
            child: CustomTextFormField(
              context: context,
              controller: _emailController,
              isEmail: true,
              hint: 'Example@123.com',
              label: 'Email',
              validator: (val) {
                final pattern =
                    new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (val != null)
                  return pattern.hasMatch(val)
                      ? null
                      : 'Masukkan e-mail yang valid';

                return null;
              },
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            height: 100,
            child: CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passwordController,
              isObscureText: _isObscurePassword,
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    var title = 'Belum punya akun? ', subTitle = 'Daftar';

    if (_isPageRegister) {
      title = 'Sudah punya akun? ';
      subTitle = 'Login';
    }

    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          setState(() {
            _isPageRegister = !_isPageRegister;
          });
        },
        child: RichText(
          text: TextSpan(
              text: title,
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: subTitle,
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      User user = User(
        email: _email,
        password: _password,
      );
      authBlocCubit.login_user(user);
    }
  }

  void handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _username = _usernameController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        _username != null) {
      User user = User(
        email: _email,
        password: _password,
        userName: _username,
      );
      authBlocCubit.register_user(user);
    }
  }
}
