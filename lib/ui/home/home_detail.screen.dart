import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeDetailScreen extends StatelessWidget {
  const HomeDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie Detail'),
      ),
      body:
          BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
        if (state is HomeBlocDetailState) {
          return Container(
            child: SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 8.0),
                      alignment: Alignment.center,
                      child: Image.network(
                        state.data.i.imageUrl,
                        height: 250,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      '${state.data.l} (${state.data.year})',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    SizedBox(height: 8),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: state.data.series?.length ?? 0,
                      itemBuilder: (context, index) {
                        var itemSeries = state.data.series[index];
                        return Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 8.0),
                              alignment: Alignment.center,
                              child: Image.network(
                                itemSeries.i.imageUrl,
                                height: 250,
                              ),
                            ),
                            Text('${itemSeries.l}'),
                          ],
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          );
        }

        return Center(
            child: Text(kDebugMode ? "state not implemented $state" : ""));
      }),
    );
  }
}
