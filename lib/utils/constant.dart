class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class DatabaseLocal {
  static const DB_NAME = "rachman_majoo.db";
  static const TBL_USER = "User";
}
